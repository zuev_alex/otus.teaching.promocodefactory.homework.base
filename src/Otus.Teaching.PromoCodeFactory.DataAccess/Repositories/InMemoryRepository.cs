﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        
        public Task<IEnumerable<T>> DeleteByIdAsync(Guid id)
        {
            //var result = Data.Where(x => x.Id != id);
            //var result1 = FakeDataFactory.Employees.Where(x => x.Id != id);
            //var result3 = Data.FirstOrDefault(x => x.Id == id);
            var result = Data.FirstOrDefault(x => x.Id == id);
            
            var data = Data.ToList();
            data.Remove(result);

            return Task.FromResult(Data = data);
        }

        public Task<IEnumerable<T>> UpdateByIdAsync(Guid id, T entity)
        {
            var data = Data.ToList();
            var result = data.FirstOrDefault(x => x.Id == id);

            data.Remove(result);
            data.Add(entity);
            
            return Task.FromResult(Data = data);
        }

        public Task<IEnumerable<T>> Create(T entity)
        {
            entity.Id = Guid.NewGuid();
            var data = Data.ToList();
            data.Add(entity);
        
            return Task.FromResult(Data = data);
        }
    }
}